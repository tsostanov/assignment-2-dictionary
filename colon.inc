%define last 0

%macro colon 2
    %if %0 != 2										; Проверка на нужное количество аргументов
        %error "Usage: colon "character", label"
    %endif
    
    %ifidn %1, ""									; Проверка на то, что есть первый аргумент (строка)
        %error "Character argument missing"			; Что бы сюда не пришло, это все равно будет считаться строкой
    %endif
    
    %ifidn %2, ""									; Проверка на то, что есть второй аргумент
        %error "Label argument missing"
    %endif

	%ifnid %2										; Проверяется, есть ли метка, указанная во втором аргументе
    	%error "The second argument of the 'colon' macro must be a label"
	%endif

    %2:
        dq last
        db %1, 0
    %define last %2
%endmacro
