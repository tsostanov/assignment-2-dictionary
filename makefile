ASM = nasm -felf64
PYTHON = python3

OBJ_DIR = obj

SRC_FILES = lib.asm dict.asm main.asm
OBJ_FILES = $(patsubst %.asm, $(OBJ_DIR)/%.o, $(SRC_FILES))

program: $(OBJ_FILES)
	ld -o program $(OBJ_FILES)

$(OBJ_DIR)/lib.o: lib.asm | $(OBJ_DIR)
	$(ASM) -o $(OBJ_DIR)/lib.o lib.asm

$(OBJ_DIR)/dict.o: dict.asm | $(OBJ_DIR)
	$(ASM) -o $(OBJ_DIR)/dict.o dict.asm

$(OBJ_DIR)/main.o: main.asm | $(OBJ_DIR)
	$(ASM) -o $(OBJ_DIR)/main.o main.asm


$(OBJ_DIR):
	mkdir -p $(OBJ_DIR)

clean:
	rm -rf $(OBJ_DIR) program

test:
	$(PYTHON) test.py

.PHONY: clean test
