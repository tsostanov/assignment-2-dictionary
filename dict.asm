%include "lib.inc"
section .text
global find_word
; Функция find_word
; Вход:
; rdi - Указатель на нуль-терминированную строку
; rsi - Указатель на начало словаря
; Выход:
; Если ключ найден, возвращается в rax адрес начала вхождения в словарь (не значения).
; Если ключ не найден, возвращается 0.
find_word:
	.comparison:
		add rsi, 8			; Так как сначала указатель
		call string_equals
		test rax, rax
		jz .dict_next
		jmp .happy_ending
	.dict_next:
		mov rsi, [rsi-8]
		test rsi, rsi
		jz .bad_ending
		jmp .comparison
	.happy_ending:
		mov rax, rsi
		ret
	.bad_ending:
		xor rax, rax
		ret


