# -*- coding: utf-8 -*-
import subprocess

string_255 = "A" * 255
string_256 = "A" * 256

inputs = ["last word", "second word", "first word", "unresolved_word", "I know what I want now more than anything else, and I'm ready to trade my life for it. With no regrets. The more witches we take out, the more people we save. So it's worth the effort. I wanna help, I just don't know what to do to make you happy again.If anyone ever tells me it's a mistake to have hope, well then, I'll just tell them they're wrong. And I'll keep telling them till they believe. No matter how many times it takes.", string_255, string_256]
outputs = ["You're weak, Haise", "second word explanation", "first word explanation", "", "", "", ""]
errors = ["", "", "", "Error: Word not found", "Error: Value too long", "Error: Word not found", "Error: Value too long"]

test_faults = []

for index, input_data in enumerate(inputs):
    process = subprocess.Popen(["./program"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = process.communicate(input=input_data.encode())
    out = out.decode().strip()
    err = err.decode().strip()
    expected_out = outputs[index]
    expected_err = errors[index]

    if out == expected_out and err == expected_err:
        print(f"{index + 1})OK ", end="")
    else:
        print(f"{index + 1})WA ", end="")
        test_faults.append([index, input_data, out, expected_out, err, expected_err])

print("\n")
for fault in test_faults:
    print("----------------------------")
    print(f"Wrong answer in test {fault[0] + 1} with \"{fault[1]}\" parameters.")
    print(f"Expected: (stdout) \"{fault[3]}\", Received \"{fault[2]}\"")
    print(f"          (stderr) \"{fault[5]}\", Received \"{fault[4]}\"")

