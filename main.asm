%define WRITE_CODE 1
%define STDERR 2
%define BUFFER_SIZE 255
%define ERROR_CODE -1
%define END_CODE 0
%include "words.inc"
%include "dict.inc"
%include "lib.inc"

section .rodata
    error_not_found: db "Error: Word not found", 0
    error_value_too_long: db "Error: Value too long", 0

section .bss
    stdin_buffer: resb (BUFFER_SIZE + 1)


section .text
global _start

_start:
    mov rdi, stdin_buffer
    mov rsi, BUFFER_SIZE
    call read_string
    
    test rax, rax
    jz .too_long

    mov rdi, stdin_buffer
    mov rsi, first_word
    call find_word

    test rax, rax
    jz .not_found

    mov rdi, rax
    add rdi, 8
    push rdi
    call string_length
    pop rdi
    add rdi, rax
    inc rdi
    call print_string
    mov rdi, END_CODE
    call exit

.not_found:
    mov rdi, error_not_found
    jmp .print_error
.too_long:
    mov rdi, error_value_too_long
    jmp .print_error

.print_error:
	 push rdi
	 call string_length
	 mov rdx, rax
     pop rsi
   	 mov rax, WRITE_CODE
	 mov rdi, STDERR
   	 syscall
     mov rdi, ERROR_CODE
	 call exit
    


